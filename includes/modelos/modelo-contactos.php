<?php
    if($_POST && $_POST['accion'] == 'crear') {
        require_once('../functions/bbdd.php');
        $nombre = filter_var($_POST['nombre'], FILTER_SANITIZE_STRING);
        $empresa = filter_var($_POST['empresa'], FILTER_SANITIZE_STRING);
        $telefono = filter_var($_POST['telefono'], FILTER_SANITIZE_STRING);

        try {
            $stmt = $connection->prepare("INSERT INTO contactos (nombre, empresa, telefono) VALUES (?,?,?)");
            $stmt->bind_param("sss", $nombre, $empresa, $telefono);
            $stmt->execute();
            if($stmt->affected_rows == 1) {
                $respuesta = array(
                    'respuesta' => 'ok',
                    'datos' => array(
                        'id' => $stmt->insert_id,
                        'nombre' => $nombre,
                        'empresa' => $empresa,
                        'telefono' => $telefono
                    )
                );
            } else {
                $respuesta = array(
                    'respuesta' => 'ko'
                );
            }
            $stmt->close();
            $connection->close();
        } catch (Exception $e) {
            $respuesta = array(
                'error' => $e->getMessage()
            );
        }
        echo json_encode($respuesta);
    }

    if($_GET && $_GET['accion'] == 'borrar') {
        require_once('../functions/bbdd.php');
        $id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);
        $accion = filter_var($_GET['accion'], FILTER_SANITIZE_STRING);

        try {
            $stmt = $connection->prepare("DELETE FROM contactos WHERE id = ?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            if($stmt->affected_rows == 1) {
                $respuesta = array(
                    'respuesta' => 'ok'
                );
            }
            $stmt->close();
            $connection->close();
        } catch (Exception $e) {
            $respuesta = array(
                'error' => $e->getMessage()
            );
        }
        echo json_encode($respuesta);
    }

    if($_POST && $_POST['accion'] == 'editar') {
        require_once('../functions/bbdd.php');
        $nombre = filter_var($_POST['nombre'], FILTER_SANITIZE_STRING);
        $empresa = filter_var($_POST['empresa'], FILTER_SANITIZE_STRING);
        $telefono = filter_var($_POST['telefono'], FILTER_SANITIZE_STRING);
        $id = filter_var($_POST['id'], FILTER_SANITIZE_STRING);

        try {
            $stmt = $connection->prepare("UPDATE contactos SET nombre = ?, empresa = ?, telefono = ? WHERE id = ?");
            $stmt->bind_param("sssi", $nombre, $empresa, $telefono, $id);
            $stmt->execute();
            if($stmt->affected_rows == 1) {
                $respuesta = array(
                    'respuesta' => 'ok',
                    'datos' => array(
                        'id' => $id,
                        'nombre' => $nombre,
                        'empresa' => $empresa,
                        'telefono' => $telefono
                    )
                );
            } else {
                $respuesta = array(
                    'error' => $e->getMessage()
                );
            }
            $stmt->close();
            $connection->close();
        } catch (Exception $e) {
            $respuesta = array(
                'respuesta' => 'ko'
            );
        }
        echo json_encode($respuesta);
    }
?>