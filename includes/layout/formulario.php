<?php $url = $_SERVER['PHP_SELF']; ?>
<div class="campos">
    <div class="campo">
        <label for="nombre">Nombre:</label>
        <input 
            type="text" placeholder="Nombre contacto" id="nombre"
            value="<?php echo ($url == '/agendaphp/editar.php') ? $contacto['nombre'] : ''; ?>">
    </div>
    <div class="campo">
        <label for="empresa">Empresa:</label>
        <input 
            type="text" placeholder="Nombre empresa" id="empresa"
            value="<?php echo ($url == '/agendaphp/editar.php')  ? $contacto['empresa'] : ''; ?>">
    </div>
    <div class="campo">
        <label for="telefono">Teléfono:</label>
        <input 
            type="tel" placeholder="Teléfono contacto" id="telefono"
            value="<?php echo ($url == '/agendaphp/editar.php')  ? $contacto['telefono'] : ''; ?>">
    </div>
</div>
<div class="campo enviar">
    <?php
        $textoBtn = ($url == '/agendaphp/editar.php') ? 'Guardar' : 'Añadir';
        $accion = ($url == '/agendaphp/editar.php') ? 'editar' : 'crear';
    ?>
    <input type="hidden" id="accion" value="<?php echo $accion; ?>">
    <?php if($url == '/agendaphp/editar.php') { ?>
        <input type="hidden" id="id" value="<?php echo $contacto['id']; ?>">
    <?php } ?>
    <input type="submit" value="<?php echo $textoBtn; ?>">
</div>