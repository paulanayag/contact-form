<?php 
include 'includes/layout/header.php';
include 'includes/functions/consultas.php';

if($_GET && $_GET['id']) {
    $id = filter_var($_GET['id'], FILTER_VALIDATE_INT);
    if(!$id) {
        die('Id no válido');
    } else {
        $resultado = obtenerContacto($id);
        if ($resultado) {
            $contacto = $resultado->fetch_assoc();
        }
    }
    
}
?>

<div class="contenedor-barra">
    <div class="contenedor barra">
        <a href="index.php" class="btn volver">Volver</a>
        <h1>Editar contacto</h1>
    </div>
</div>
<div class="bg-formulario contenedor sombra">
    <form id="contacto" action="#">
        <legend>Edita el contacto</legend>
        <?php include 'includes/layout/formulario.php';?>
    </form>
</div>

<?php include 'includes/layout/footer.php';?>