const formularioContactos = document.querySelector('#contacto');
const tablaContactos = document.querySelector('#tabla-contactos tbody');
const inputBuscador = document.querySelector('#buscar');

eventListener();

function eventListener() {
    if (formularioContactos) {
        formularioContactos.addEventListener('submit', leerFormulario);
    }
    if(tablaContactos) {
        tablaContactos.addEventListener('click', eliminarContacto);
        calcularTotalContactos();
    }
    if(inputBuscador) {
        inputBuscador.addEventListener('input', buscarContactos);
    }
}

function leerFormulario(e) {
    e.preventDefault();
    const nombre = document.querySelector('#nombre').value;
    const empresa = document.querySelector('#empresa').value;
    const telefono = document.querySelector('#telefono').value;
    const accion = document.querySelector('#accion').value;
    

    if(nombre === '' || empresa === '' || telefono === '') {
        mostrarNotificacion('Todos los campos son obligatorios', 'ko');
    } else {
        const infoContacto = new FormData();
        infoContacto.append('nombre', nombre);
        infoContacto.append('empresa', empresa);
        infoContacto.append('telefono', telefono);
        infoContacto.append('accion', accion);
        //console.log(...infoContacto);

        if(accion === 'crear') {
            insertarContactoBBDD(infoContacto);
        } else if (accion === 'editar'){
            const idRegistro = document.querySelector('#id').value;
            infoContacto.append('id', idRegistro);
            editarRegistro(infoContacto);
        }
    }
}

function mostrarNotificacion(mensaje, clase) {
    const  notificacion = document.createElement('div');
    notificacion.classList.add(clase, 'notificacion', 'sombra');
    notificacion.textContent = mensaje;
    formularioContactos.insertBefore(notificacion, document.querySelector('form legend'));
    setTimeout(() => {
        notificacion.classList.add('visible');
        setTimeout(() => {
            notificacion.classList.remove('visible');
            setTimeout(() => {
                notificacion.remove();
            }, 500);
        }, 3000)
    }, 100)
}

function insertarContactoBBDD(infoContacto) {
    // Instancia objeto XML
    const xhr = new XMLHttpRequest();

    // Conexión
    xhr.open('POST', 'includes/modelos/modelo-contactos.php', true);

    // Paso datos
    xhr.onload = function() {
        if(this.status === 200) {
            const respuesta = JSON.parse(xhr.responseText);
            console.log(respuesta);
            const nuevoContacto = document.createElement('tr');
            const contenedorAcciones = document.createElement('td');
            const iconoEditar = document.createElement('i');
            const btnEditar = document.createElement('a');
            const iconoBorrar = document.createElement('i');
            const btnBorrar = document.createElement('button');

            nuevoContacto.innerHTML = `
                <td>${respuesta.datos.nombre}</td>
                <td>${respuesta.datos.empresa}</td>
                <td>${respuesta.datos.telefono}</td>
            `;

            añadirBoton(iconoEditar, btnEditar, 'fas', 'fa-edit', 'btn-editar');
            btnEditar.href = `editar.php?id=${respuesta.datos.id}`;

            añadirBoton(iconoBorrar, btnBorrar, 'fas', 'fa-trash-alt', 'btn-borrar');
            btnBorrar.setAttribute('data-id', respuesta.datos.id);

            contenedorAcciones.appendChild(btnEditar);
            contenedorAcciones.appendChild(btnBorrar);
            nuevoContacto.appendChild(contenedorAcciones);
            tablaContactos.appendChild(nuevoContacto);

            document.querySelector('form').reset();

            mostrarNotificacion('Contacto creado correctamente', 'ok');
            calcularTotalContactos();
        }
    }

    // Envío datos
    xhr.send(infoContacto);
}

function añadirBoton(icono, btn, clase1, clase2, claseBtn) {
    icono.classList.add(clase1, clase2);
    btn.appendChild(icono);
    btn.classList.add('btn', claseBtn);
}

function eliminarContacto(e) {
    console.log(e.target);
    if(e.target.parentElement.classList.contains('btn-borrar')) {
        const id = e.target.parentElement.getAttribute('data-id');
        const respuesta = confirm('¿Está seguro de que desea eliminar el contacto?');
        if(respuesta) {
            if(id) {
                const xhr = new XMLHttpRequest();

                xhr.open('GET', `includes/modelos/modelo-contactos.php?id=${id}&accion=borrar`, true);
    
                xhr.onload = function() {
                    if(this.status === 200) {
                        const respuesta = JSON.parse(xhr.responseText);
                        if(respuesta.respuesta === 'ok') {
                            e.target.parentElement.parentElement.parentElement.remove();
                            mostrarNotificacion('Se ha eliminado correctamente', 'ok');
                            calcularTotalContactos();
                        } else {
                            mostrarNotificacion('Se ha producido un error...', 'ko');
                        }
                    }
                }
    
                xhr.send();
            } else {
                mostrarNotificacion('Se ha producido un error...', 'ko');
            }
            
        }
    }
}

function editarRegistro(info) {
    const xhr = new XMLHttpRequest();

    xhr.open('POST', 'includes/modelos/modelo-contactos.php', true);

    xhr.onload = function () {
        if(this.status === 200) {
            const respuesta = JSON.parse(xhr.responseText);
            if(respuesta.respuesta === 'ok') {
                mostrarNotificacion('Contacto editado correctamente', 'ok');
                setTimeout(() => {
                    window.location.href = 'index.php';
                }, 3500);
            } else {
                mostrarNotificacion('No se ha podido guardar', 'ko');
            }
        }
    }

    xhr.send(info);
}

function buscarContactos(e) {
    const expresion = new RegExp(e.target.value, "i");
    const registros = document.querySelectorAll('tbody tr');

    registros.forEach(registro => {
        registro.style.display = 'none';
        if(registro.childNodes[1].textContent.replace(/\s/g, " ").search(expresion) != -1) {
            registro.style.display = 'table-row';
        }
        calcularTotalContactos();
    });
}

function calcularTotalContactos() {
    const totalContactos = document.querySelectorAll('tbody tr');
    const numContactos = document.querySelector('.total-contactos span');
    let total = 0;

    totalContactos.forEach(contacto => {
        if(contacto.style.display !== 'none') {
            total++;
        }
    });
    numContactos.textContent = total;
}