<?php include 'includes/layout/header.php';?>
<?php include 'includes/functions/consultas.php';?>

<div class="contenedor-barra">
    <h1>Agenda de contactos</h1>
</div>
<div class="bg-formulario contenedor sombra">
    <form id="contacto" action="#">
        <legend>
            Añade un nuevo contacto <span>Todos los campos son obligatorios</span>
        </legend>
        <?php include 'includes/layout/formulario.php';?>
    </form>
</div>
<div class="bg-tabla-contactos contenedor sombra contactos">
    <div class="contenedor-contactos">
        <h2>Contactos</h2>
        <input type="text" id="buscar" class="buscador sombra" placeholder="Buscar contactos...">
        <p class="total-contactos"><span></span> contactos</p>
        <table id="tabla-contactos" class="tabla-contactos">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Empresa</th>
                    <th>Teléfono</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $contactos = obtenerContactos();
                    if($contactos->num_rows) { 
                        foreach($contactos as $contacto) { ?>
                        <tr>
                            <td><?php echo $contacto['nombre']; ?></td>
                            <td><?php echo $contacto['empresa']; ?></td>
                            <td><?php echo $contacto['telefono']; ?></td>
                            <td>
                                <a href="editar.php?id=<?php echo $contacto['id']; ?>" class="btn-editar btn">
                                    <i class="fas fa-edit"></i>
                                </a>
                                <button data-id="<?php echo $contacto['id']; ?>" type="button" class="btn-borrar btn">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </td>
                        </tr>
                        <?php } ?>
                    <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<?php include 'includes/layout/footer.php';?>